import traceback
import requests
from selenium.webdriver.chrome.options import Options


def retry(func, max_count=3):
    retry_count = 0

    while True:
        if (retry_count >= max_count):
            raise Exception('Reach maximun retry count')

        try:
            func()
            break
        except:
            traceback.print_exc()
            retry_count = retry_count + 1


def notify_slack(message, hook_url):
    requests.post(hook_url, json={'text': message})


def get_headless_options():
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument("--no-sandbox")

    return chrome_options
