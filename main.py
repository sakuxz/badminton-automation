import os
import sys
import time
from selenium import webdriver
from sport_center import book, login
from utils import get_headless_options, retry, notify_slack
from dotenv import load_dotenv
from datetime import datetime, timezone, timedelta
import arrow


places = {
    "five20": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(14) > td:nth-child(4) > img",
    "seven20": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(15) > td:nth-child(4) > img",
    "night20": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(16) > td:nth-child(4) > img",
    "six20": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(17) > td:nth-child(4) > img",
    "eight20": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(18) > td:nth-child(4) > img",
    "ten20": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(19) > td:nth-child(4) > img",

    "five21": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(20) > td:nth-child(4) > img",
    "seven21": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(21) > td:nth-child(4) > img",
    "night21": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(22) > td:nth-child(4) > img",
    "six21": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(23) > td:nth-child(4) > img",
    "eight21": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(24) > td:nth-child(4) > img",
    "ten21": "#ContentPlaceHolder1_Panel_Step2 > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(25) > td:nth-child(4) > img",
}
target_place = places[sys.argv[1]]

today = arrow.utcnow().to("Asia/Taipei")
midnight = today.shift(days=1).floor('day')
target_date = today.shift(days=14)

load_dotenv()
sport_center_url = os.getenv("SPORT_CENTER_URL")
username = os.getenv("USERNAME")
password = os.getenv("PASSWORD")
slack_hook_url = os.getenv("SLACK_HOOK_URL")


if __name__ == '__main__':
    driver = webdriver.Chrome(options=get_headless_options())
    driver.implicitly_wait(8)

    try:
        retry(lambda: login(driver, sport_center_url, username, password))
        time.sleep((midnight - arrow.utcnow()).total_seconds())
        print("start booking " + str(arrow.utcnow().to("Asia/Taipei")))
        retry(lambda: book(driver, sport_center_url, target_place, target_date))

        notify_slack("預約成功", slack_hook_url)
    finally:
        driver.close()
