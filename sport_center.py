import cv2
import pytesseract
import uuid


def _create_temp_file_path():
    return '/tmp/' + str(uuid.uuid4()) + '.png'


def _get_captcha_text(image_path):
    image = cv2.imread(image_path)
    denoised_image = cv2.fastNlMeansDenoisingColored(image, None, 30, 30)
    _, preprocessed_image = cv2.threshold(
        denoised_image, 65, 255, cv2.THRESH_BINARY)

    tmp_file_path = _create_temp_file_path()
    cv2.imwrite(tmp_file_path, preprocessed_image)

    return pytesseract.image_to_string(tmp_file_path).split('\n')[0]


def login(driver, sport_center_url, username, password):
    driver.get(sport_center_url + "?module=login_page&files=login")

    alert = driver.switch_to.alert
    alert.accept()
    alert = driver.switch_to.alert
    alert.accept()

    driver.find_element_by_id(
        'ContentPlaceHolder1_loginid').send_keys(username)
    driver.find_element_by_id('loginpw').send_keys(password)

    captcha = driver.find_element_by_id('ContentPlaceHolder1_CaptchaImage')
    captcha_image_bytes = captcha.screenshot_as_png

    tmp_file_path = _create_temp_file_path()
    f = open(tmp_file_path, 'wb')
    f.write(captcha_image_bytes)
    f.close()

    driver.find_element_by_id(
        'ContentPlaceHolder1_Captcha_text').send_keys(_get_captcha_text(tmp_file_path))
    driver.find_element_by_id('login_but').click()

    assert driver.find_element_by_id('member_login').text == '[登出]'


def book(driver, sport_center_url, book_btn_css_selector, target_date):
    url = sport_center_url + "?module=net_booking&files=booking_place&StepFlag=2&PT=1&D=" + \
        target_date.format("YYYY/MM/DD") + "&D2=3"
    print(url)
    driver.get(url)

    book_btn = driver.find_element_by_css_selector(
        book_btn_css_selector)
    print(book_btn.get_attribute('title'))
    book_btn.click()

    alert = driver.switch_to.alert
    alert.accept()

    result_text = driver.find_element_by_css_selector(
        "#ContentPlaceHolder1_Step3Info_lab > span:nth-child(1)").text
    print(result_text)
    assert result_text == "預約成功"
